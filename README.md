docker-intel
============

[![pipeline status](https://gitlab.com/mmoelle1/docker-intel/badges/master/pipeline.svg)](https://gitlab.com/mmoelle1/docker-intel/commits/master)

Repository contains a customizable `Dockerfile` based on
`ubuntu:20.04` with **Intel's oneAPI compilers**. The
generated docker images moreover contain the tools `cmake`,
`doxygen`, `git`, `ninja`, `svn`, `wget` and the libraries
`arrayfire`, `blas`, `boost`, and `lapack`. All containers
contain the Intel OpenCL driver for Intel CPUs.

Available docker images:

**Ubuntu 20.04**
- [registry.gitlab.com/mmoelle1/docker-intel:ubuntu:20.04](registry.gitlab.com/mmoelle1/docker-intel:ubuntu20.04)
